let coins;
let output = {};
let funcStatus = [];

const calculatingDenominations = input => {
    let largestPossibleCoin = 1;
    if (input) {
        funcStatus.status = 'success';
        for (const key in coins) {
            if (input >= key) {
                if (coins[key] > 0) {
                    largestPossibleCoin = key;
                }
            }
        }
        
        if (coins[largestPossibleCoin] === 0) {
            funcStatus.status = 'false';
            return true;
        }


        let remainingValue = input % largestPossibleCoin;
        let qte = ((input - remainingValue) / largestPossibleCoin);

        if (qte > coins[largestPossibleCoin]) {
            remainingValue = input - (largestPossibleCoin * coins[largestPossibleCoin]);
            output[largestPossibleCoin / 100] = coins[largestPossibleCoin];
            coins[largestPossibleCoin] -= coins[largestPossibleCoin];
        } else {
            remainingValue = input - (largestPossibleCoin * qte);
            output[largestPossibleCoin / 100] = qte;
            coins[largestPossibleCoin] = coins[largestPossibleCoin] - qte;
        }

        if (remainingValue > 0) {
            calculatingDenominations(remainingValue);
        }
        
    }
    return funcStatus;
}

const givingChange = (value, qte) => {

    output = {};
    
    if ((isNaN(value) || +value <= 0) || (isNaN(qte) || +qte <= 0)) {
        output.status = 'false';
        return output;
    } else if (value < (4.98 * qte)) {
        output.status = 'notEnough';
        return output;
    } else {
        value = (Number(value) * 100) - (498 * qte);
    }
    
    coins = {
        1: 10,
        50: 20,
        10000: 1,
        500: 5,
        2: 10,
        5: 0,
        10: 7,
        20: 4,
        100: 13,
        200: 3,
        1000: 4,
        2000: 8,
        5000: 2,
        20000: 1
    };
    
    coins = Object.entries(coins)
        .sort(([,a],[,b]) => a-b)
        .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

    let returnValue = calculatingDenominations(value);
    
    if (returnValue.status === 'success') {
        return output;
    } else {
        output = {};
        output.status = "error";
        return output;
    }
}

let clickMe = document.querySelector('#calc');

clickMe.addEventListener('click', function () {
    let outputString = '';
    let qte = document.querySelector('.qte');
    let count = document.querySelector('.money');
    let text = document.querySelector('.result-list');
	let result = givingChange(count.value, qte.value);

    if (!result.status) {
        outputString += `<ul>`;
        Object.keys(output).forEach((key) => {
            outputString += `<li>ilość: ${output[key]}, nominał: ${key} zł</li>`;
        });
        outputString += `</ul>`;
    } else {
        let st = result.status;
        console.log(st);
        switch (st) {
            case 'notEnough':
                outputString += `<p>Wrzuciłeś za mało pieniędzy</p>`;
                break;
            case 'false':
                outputString += `<p>Należy podać tylko cyfry</p>`;
                break;
        }
    }
    
    text.innerHTML  = outputString;

    return true;

});